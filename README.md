<!-- LTeX: language=en -->
# findups, find duplicate (plagiarism) files in a set of directories

## Purpose

Most plagiarism detection software try to find similarities between projects, assuming students change the code to avoid being caught. This is good, but another use-case is to find files that students did not modify at all. This is common when students must write many files, and changing each file is time-consuming for them, hence they leave some or all files unmodified.

findups allows easily finding such exact duplicate, filtering-out false positive, and presenting them in a form which makes it easy to investigate actual plagiarism vs coincidence.

## Features

* Pattern-based file selection, e.g. find duplicates among all `.c` files using `findups.py '**/*.c'`.

* Filter according to the number of duplicates of each file with e.g. `--dups-filter 'count < 50 and count >= 2'`. If there are too many duplicates, it is probably legitimate. If there's just one, it's not a duplicate actually ;-) (hence the default filter is `count >= 2`).

* Filter according to file name, size, content with e.g. `--files-filter 'nb_lines >= 3 and "provided" not in path and grep("foo")`. Very small files are probably not interesting. You may want to focus on a particular subset of the files where you know plagiarism is more likely or more easily provable.

* Group files per directory with `--group LEVEL`, so that duplicates within the same project are not counted.

* Output in the form of a directory + symlinks hierarchy. View duplicates either sorted by number of duplicates, or in the original files hierarchy.

* Take file's last modification date into account in complement, to flag the most likely plagiarism (almost no false positive in this category).

## Output format

The output is a directory + symlinks hierarchy, looking like:

```
$ findups.py --output-dir dups/ 'test/stu*/**/*.txt'
$ tree dups       
dups/
├── 2
│   ├── 55
│   │   ├── 1-global-dup.txt -> /home/mmoy/enseignements/tools/findups/test/student2/global-dup.txt
│   │   └── 2-global-dup.txt -> /home/mmoy/enseignements/tools/findups/test/student1/global-dup.txt
│   ├── a5
│   │   ├── 1-original.txt -> /home/mmoy/enseignements/tools/findups/test/student1/original.txt
│   │   └── 2-copy.txt -> /home/mmoy/enseignements/tools/findups/test/student3/copy.txt
│   └── bb
│       ├── 1-local-dup2.txt -> /home/mmoy/enseignements/tools/findups/test/student1/local-dup2.txt
│       └── 2-local-dup1.txt -> /home/mmoy/enseignements/tools/findups/test/student1/local-dup1.txt
├── 3
│   └── a7
│       ├── 1-everyone.txt -> /home/mmoy/enseignements/tools/findups/test/student3/everyone.txt
│       ├── 2-everyone.txt -> /home/mmoy/enseignements/tools/findups/test/student2/everyone.txt
│       └── 3-everyone.txt -> /home/mmoy/enseignements/tools/findups/test/student1/everyone.txt
├── files
│   └── test
│       ├── student1
│       │   ├── everyone-1.txt -> /home/mmoy/enseignements/tools/findups/test/student3/everyone.txt
│       │   ├── everyone-2.txt -> /home/mmoy/enseignements/tools/findups/test/student2/everyone.txt
│       │   ├── global-dup-1.txt -> /home/mmoy/enseignements/tools/findups/test/student2/global-dup.txt
│       │   ├── local-dup1-1.txt -> /home/mmoy/enseignements/tools/findups/test/student1/local-dup2.txt
│       │   ├── local-dup2-1.txt -> /home/mmoy/enseignements/tools/findups/test/student1/local-dup1.txt
│       │   └── original-1.txt -> /home/mmoy/enseignements/tools/findups/test/student3/copy.txt
│       ├── student2
│       │   ├── everyone-1.txt -> /home/mmoy/enseignements/tools/findups/test/student3/everyone.txt
│       │   ├── everyone-2.txt -> /home/mmoy/enseignements/tools/findups/test/student1/everyone.txt
│       │   └── global-dup-1.txt -> /home/mmoy/enseignements/tools/findups/test/student1/global-dup.txt
│       └── student3
│           ├── copy-1.txt -> /home/mmoy/enseignements/tools/findups/test/student1/original.txt
│           ├── everyone-1.txt -> /home/mmoy/enseignements/tools/findups/test/student2/everyone.txt
│           └── everyone-2.txt -> /home/mmoy/enseignements/tools/findups/test/student1/everyone.txt
└── files-sametime
    └── test
        ├── student1
        │   ├── global-dup-1.txt -> /home/mmoy/enseignements/tools/findups/test/student2/global-dup.txt
        │   ├── local-dup1-1.txt -> /home/mmoy/enseignements/tools/findups/test/student1/local-dup2.txt
        │   └── local-dup2-1.txt -> /home/mmoy/enseignements/tools/findups/test/student1/local-dup1.txt
        └── student2
            └── global-dup-1.txt -> /home/mmoy/enseignements/tools/findups/test/student1/global-dup.txt

15 directories, 25 files
```

Numbered directories contain duplicates grouped by number of duplicates. Directory `2/` list files occurring exactly twice, `3/` files appearing exactly 3 times, ... The next level of the hierarchy is a hash of the files' content, e.g. `55` is the hash of both `/path/to/test/student2/global-dup.txt` and `/path/to/test/student1/global-dup.txt`, which are therefore duplicates (the hash length is adapted automatically to avoid conflict, hence same hash = same content).

The `files` directory matches roughly the original hierarchy, but contains only duplicates, and each file is replaced with symlinks to all duplicates of the file (a `-1`, `-2`, ... suffix is added to the file name hence `one-1.txt` means "the first duplicate of file `one.txt`"), which is `two.txt` (destination of the symlink).

The `files-samedate` directory is like `files` but contains only duplicates with the same modification date (+/- 2 seconds, in case moving from a filesystem to another with different timing resolution yields rounding errors). Files in this category are almost certainly copied with a tool preserving modification dates (ZIP or tar file, `cp`, `rsync -a`, copy in graphical browser, etc.). The converse is not true, looking only at this directory may miss a lot of actual plagiarism.

One usually want to consider duplicates within a project as legitimate, hence consider different directories as different projects, using `--group N` where `N` is the number of directory levels to consider as the project name. For example, `--group 1` means that different directories are different projects, `--group 2` considers `foo/bar/boz.txt` and `foo/bar/biz.txt` to be in the same project, but not `foo/notbar/baz.txt`. When doing so, findups counts the number of duplicates contained within each project, and prefixes the output directories with this number.

```
$ ./findups.py --output-dir dups/ 'test/stu*/**/*.txt' --group 2
$ tree dups/files/
dups/files/
├── 002-test__student2
│   ├── everyone-1.txt -> /path/to/test/student3/everyone.txt
│   ├── everyone-2.txt -> /path/to/test/student1/everyone.txt
│   └── global-dup-1.txt -> /path/to/test/student1/global-dup.txt
├── 002-test__student3
│   ├── copy-1.txt -> /path/to/test/student1/original.txt
│   ├── everyone-1.txt -> /path/to/test/student2/everyone.txt
│   └── everyone-2.txt -> /path/to/test/student1/everyone.txt
└── 003-test__student1
    ├── everyone-1.txt -> /path/to/test/student2/everyone.txt
    ├── everyone-2.txt -> /path/to/test/student3/everyone.txt
    ├── global-dup-1.txt -> /path/to/test/student2/global-dup.txt
    └── original-1.txt -> /path/to/test/student3/copy.txt
```

`test/student1` contains 3 duplicates (hence the `003-` prefix), which are located at `test/student1/everyone.txt` (with two duplicates, hence the `-1.txt` and `-2.txt` suffixes), `test/student1/global-dup.txt` and `test/student1/original.txt`.

Directory with the highest number are the most likely to be plagiarism.

## Files Filters

One can focus on a particular subset of files using `--files-filter FILTER`. `FILTER` is an arbitrary Python Boolean expression (do not feed user input, security hasard). The expression is evaluated for each file, and only files for which the expression is true are considered for duplicate searching.

Since this is a Python expression, classical operators `and`, `or`, `not`, ... are allowed.

Special primitives are also available :

* `path` is the path to the current file. Example: `--files-filter '"provided" not in path'`.

* `grep(pattern)` is true iff one of the line matches `pattern` (a regular expression).

* `size` is the file size, e.g. `--files-filter "size > 10"` for files more than 10 bytes.

* `nb_lines` is the number of lines in the file, e.g. `--files-filter "nb_lines >= 3"` for files containing 3 lines or more.

* `trailing_sp` is true iff the file contains trailing spaces. `mix_tab_sp` iff the file contains tab/space mix (a tab followed by a space, or vice versa). `weird_sp` is true if either of the previous is true. These files are not necessarily more likely to be plagiarism, but when they are, they are usually in an easily provable way.
