#!/usr/bin/env python

# Find the latest version of this program at
# https://gitlab.com/moy/findups


# BSD 2-Clause License
#
# Copyright (c) 2017, Matthieu Moy and Guillaume Salagnac
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in
#   the documentation and/or other materials provided with the
#   distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import re
import sys
import os
import argparse
import glob
import hashlib
import mmap

__version__ = '0.1'


def make_parser():
    parser = argparse.ArgumentParser(description="""
        Takes a list of files as an extended wildcard (e.g. **/*.c), and find
        files that are exact duplicates of each other.""")
    parser.add_argument(
        "--output-dir", "-o",
        help="Output directory (where report be generated)",
        metavar="DIR", type=str)
    parser.add_argument(
        "FILES", nargs='*',
        help="Input files",
        type=str)
    parser.add_argument(
        "--exclude", metavar='GLOB', type=str, action='append',
        help="""Files that shouldn't count as duplicates (e.g. given in the
        skeleton).""")
    parser.add_argument(
        "--existing", metavar='GLOB', type=str, action='append',
        help="""Existing files that student's aren't supposed to duplicate
        (e.g. last year's lab).""")
    parser.add_argument(
        "--group", metavar='LEVELS', type=int, default=0,
        help="""Don't consider duplicates that have more than LEVELS levels of
        directory hierarchy in common (typically to skip duplicates within the
        same project)."""
    )
    parser.add_argument(
        '--verbose', '-v', action='store_true',
        help="Be verbose about what's being done"
    )
    parser.add_argument(
        '--files-filter', metavar='FILES_FILTER', type=str,
        help="""Filter out files not matching some criteria before launching
        duplicate search."""
    )
    parser.add_argument(
        '--dups-filter', metavar='DUPS_FILTER', type=str, default='count >= 2',
        help="""Filter to apply on the computed duplicate set."""
    )
    return parser


def die(*msg):
    # Don't mix stdout and stderr
    sys.stdout.flush()
    print(*msg, file=sys.stderr)
    sys.exit(1)


def sha256sum(filename):
    h = hashlib.sha256()
    if os.stat(filename).st_size == 0:
        return h.hexdigest()
    with open(filename, 'rb') as f:
        with mmap.mmap(f.fileno(), 0, prot=mmap.PROT_READ) as mm:
            h.update(mm)
    return h.hexdigest()


def parts(path):
    p, f = os.path.split(os.path.normpath(path))
    if p in ('', '/'):
        return (f,)
    else:
        return parts(p) + (f,)


def path_prefix(path, group_level):
    return os.path.sep.join(parts(path)[:group_level])


def test_path_prefix():
    assert parts('a') == ('a',)
    assert parts('a/b') == ('a', 'b')
    assert parts('/a/b') == ('a', 'b')
    assert parts('a/b/c.txt') == ('a', 'b', 'c.txt')
    assert parts('a/b/') == ('a', 'b')
    assert path_prefix('a/b/c/d.txt', 1) == 'a'
    assert path_prefix('a/b/c/d.txt', 2) == 'a/b'
    assert path_prefix('a/b/c/d.txt', 3) == 'a/b/c'
    assert path_prefix('a/b/c/d.txt', 4) == 'a/b/c/d.txt'


def build_dict(files, exclude_shas, group_level, verbose):
    files_dict = dict()
    for file in files:
        if verbose:
            print(file)
        sha = sha256sum(file)
        if sha in exclude_shas:
            if verbose:
                print("Skipping {} because of --exclude...".format(file))
            continue
        if sha not in files_dict:
            files_dict[sha] = []
        skip_this_file = False
        if group_level:
            my_prefix = path_prefix(file, group_level)
            for other_file in files_dict[sha]:
                other_prefix = path_prefix(other_file, group_level)
                if my_prefix == other_prefix:
                    skip_this_file = True
                    break
        if skip_this_file:
            if verbose:
                print("Skipping {} because of --group {}."
                      .format(file, group_level))
            continue
        files_dict[sha].append(file)
    return files_dict


def count_dups(files_dict, group_level, existing_list):
    count_dict = dict()

    # Number of duplicates found in each group
    # (identified by a path prefix)
    prefix_score = dict()
    for sha, paths in files_dict.items():
        # Duplicates that touch only existing files are irrelevant. We count a
        # duplicate when at least one path in the duplicate is not in
        # existing_list.
        new_paths = set(paths) - existing_list
        if len(new_paths) not in count_dict:
            count_dict[len(new_paths)] = []
        count_dict[len(new_paths)].append(sha)

        for path in paths:
            prefix = path_prefix(path, group_level)
            if prefix not in prefix_score:
                prefix_score[prefix] = 0
            if len(paths) > 1 and new_paths:
                prefix_score[prefix] += 1
    return count_dict, prefix_score


def make_count_symlinks_and_dirs(files_dict, count_dict,
                                 output_dir, prefix_len):
    for count, shas in count_dict.items():
        if count == 0:
            continue
        count_dir = os.path.join(output_dir, str(count))
        os.makedirs(count_dir, exist_ok=True)
        for sha in shas:
            short = sha[:prefix_len]
            sha_dir = os.path.join(count_dir, short)
            os.makedirs(sha_dir, exist_ok=True)
            count = 0
            for path in files_dict[sha]:
                count += 1
                linkname = os.path.join(sha_dir, "{}-{}".format(
                    count,
                    os.path.basename(path)))
                abspath = os.path.abspath(path)
                os.symlink(abspath, linkname)


def make_files_symlinks_and_dirs(files_dict, prefix_score,
                                 group_level, output_dir,
                                 verbose,
                                 only_sametime):
    for _, paths in files_dict.items():
        for path in paths:
            if group_level > 0:
                score = prefix_score[path_prefix(path, group_level)]
                if score == 0:
                    continue
                prefix = '{:03d}-'.format(score)
                # Flatten the first levels of directory hierarchy, so that each
                # output directory corresponds to one group in the input
                # hierarchy.
                dir = os.path.dirname(path).replace('/', '__', group_level - 1)
            else:
                prefix = ''
                dir = os.path.dirname(path)
                # if dir is absolute, we make it relative (to /), because
                # os.path.join(path1, absolute) returns absolute and ignores
                # path1. We won't want to write outside our output directory...
                if os.path.isabs(dir):
                    os.path.relpath(dir, start=os.path.abspath(os.sep))
                assert not os.path.isabs(dir) and not dir.startswith('..')
            suffix = '-sametime' if only_sametime else ''
            file_dir = os.path.join(output_dir, f'files{suffix}',
                                    f'{prefix}{dir}')
            count = 0
            others = paths.copy()
            others.remove(path)
            if only_sametime:
                m_path = os.path.getmtime(path)
            else:
                m_path = 0.0
            for other in others:
                if only_sametime:
                    m_other = os.path.getmtime(other)
                    if abs(m_other - m_path) > 2.0:
                        if verbose:
                            print(f"Skipping {other} because they "
                                  f"have different dates "
                                  f"({abs(m_other - m_path)}).")
                        continue
                os.makedirs(file_dir, exist_ok=True)
                count += 1
                base, ext = os.path.basename(path).rsplit('.', 1)
                linkname = os.path.join(file_dir, "{}-{}.{}".format(
                    base,
                    count,
                    ext))
                abspath = os.path.abspath(other)
                os.symlink(abspath, linkname)


def shortest_prefix(files_dict):
    # brute-force algo: check for conflicts with each prefix length, and take
    # the first one without conflict.
    for candidate_len in range(2, 100):

        def has_conflict(files_dict, candidate_len):
            conflicts = dict()
            for sha in files_dict:
                prefix = sha[:candidate_len]
                if prefix in conflicts:
                    return True
                conflicts[prefix] = True
            return False

        if not has_conflict(files_dict, candidate_len):
            return candidate_len

    raise AssertionError("Err, still conflicts with full lenght??")


def test_shortest_prefix():
    assert shortest_prefix({'abcd': 1, 'abce': 2}) == 4
    assert shortest_prefix({'abcd123': 1, 'abce123': 2}) == 4


def size(file):
    return os.stat(file).st_size


def nb_lines(file):
    with open(file, errors='surrogateescape') as handle:
        return sum(1 for _ in handle)


def grep(file, pattern):
    with open(file, errors='surrogateescape') as handle:
        return any(re.search(pattern, line) for line in handle.readlines())


def filter_files(files, filter):
    if filter is None:
        return files
    return (f for f in files if eval(filter, {
        'size': size(f),
        'nb_lines': nb_lines(f),
        'grep': lambda pattern: grep(f, pattern),
        'trailing_sp': grep(f, r'[ \t]$'),
        'mix_tab_sp': grep(f, '(\t | \t)'),
        'weird_space': grep(f, r'[ \t]$') or grep(f, '(\t | \t)'),
        'path': f})
    )


def test_filter_files():
    paths_oneline = ('test/one.txt', 'test/two.txt', 'test/three.txt')
    paths_multilines = ('test/3-lines.txt', 'test/4-lines.txt')
    paths = paths_oneline + paths_multilines
    assert tuple(filter_files(paths, 'size > 2')) == paths_multilines
    assert tuple(filter_files(paths, 'size < 3')) == paths_oneline
    assert tuple(filter_files(paths, 'nb_lines <= 1')) == paths_oneline
    assert tuple(filter_files(paths, 'nb_lines > 1 and nb_lines <= 3')) == (
        'test/3-lines.txt',)
    assert tuple(filter_files(paths, 'grep("one")')) == paths_multilines
    assert tuple(filter_files(paths, 'grep("four")')) == ('test/4-lines.txt',)
    assert tuple(filter_files(paths, 'grep("our")')) == ('test/4-lines.txt',)
    assert tuple(filter_files(paths, 'grep("fou")')) == ('test/4-lines.txt',)


def filter_dups(dups_dict, filter):
    if filter is None:
        return dups_dict
    result = dict()
    for sha, paths in dups_dict.items():
        if eval(filter, {
            'count': len(paths)
        }):
            result[sha] = paths
    return result


def test_filter_dups():
    d = {'a': ('test/one.txt', 'test/two.txt'),
         'b': ('test/three.txt',),
         'c': ('test/3-lines.txt',),
         'd': ('test/4-lines.txt',)}
    assert filter_dups(d, 'count >= 2') == {'a': d['a']}
    assert filter_dups(d, 'count <= 2') == d
    assert filter_dups(d, 'count < 2') == {
        'b': d['b'],
        'c': ('test/3-lines.txt',),
        'd': ('test/4-lines.txt',)
    }


def shas(paths):
    return {sha256sum(path) for path in paths}


def findups(files, exclude_list, existing_list, output_dir, group_level,
            verbose,
            files_filter, dups_filter):
    print("Applying files filters ...")
    filtered_files = filter_files(files, files_filter)
    print("Reading exclude list ...")
    exclude_shas = shas(exclude_list)
    print("Searching for duplicates ...")
    files_dict = build_dict(
        set(filtered_files).union(existing_list),
        exclude_shas, group_level, verbose
    )
    if files_dict == dict():
        print("No duplicate found.")
        return
    files_dict = filter_dups(files_dict, dups_filter)
    if files_dict == dict():
        print("All duplicates found filtered out.")
        return
    count_dict, prefix_score = count_dups(
        files_dict, group_level, existing_list)
    prefix_len = shortest_prefix(files_dict)
    make_count_symlinks_and_dirs(
        files_dict, count_dict, output_dir, prefix_len)
    for only_sametime in (True, False):
        make_files_symlinks_and_dirs(
            files_dict, prefix_score, group_level, output_dir,
            verbose, only_sametime)


def expand_pattern_list(pattern_list):
    result = set()
    if pattern_list:
        for pattern in pattern_list:
            result.update(glob.glob(os.path.expanduser(pattern),
                          recursive=True))
    return result


def main():
    parser = make_parser()
    opts = parser.parse_args()

    if opts.output_dir is None:
        die("Please specify output directory with --output-dir DIR.")
    if os.path.exists(opts.output_dir):
        die("Path {} already exists.".format(opts.output_dir))

    files = set()

    if len(opts.FILES) == 0:
        die("Please specify a pattern with e.g. {} '**/*.c'.".format(
            sys.argv[0]))
    for pattern in opts.FILES:
        files.update(glob.glob(pattern, recursive=True))
    if files == set():
        if len(opts.FILES) > 1:
            die("no file match patterns {}.".format(', '.join(opts.FILES)))
        else:
            die("no file match pattern {}.".format(', '.join(opts.FILES)))

    print("Listing files matching patterns (exclude) ...", end='')
    exclude_list = expand_pattern_list(opts.exclude)
    if opts.exclude and len(exclude_list) == 0:
        print("ERROR: no files found, remove the --exclude option "
              "or check your pattern.")
        sys.exit(1)
    print(f" {len(exclude_list)} files found.")
    print("Listing files matching patterns (existing) ...", end='')
    existing_list = expand_pattern_list(opts.existing)
    if opts.existing and len(existing_list) == 0:
        print("ERROR: no files found, remove the --existing option "
              "or check your pattern.")
        sys.exit(1)
    print(f" {len(existing_list)} files found.")

    findups(files, exclude_list, existing_list, opts.output_dir, opts.group,
            opts.verbose, opts.files_filter, opts.dups_filter)


if __name__ == '__main__':
    main()
